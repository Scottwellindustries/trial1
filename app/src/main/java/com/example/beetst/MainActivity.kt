package com.example.beetst

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.spotify.android.appremote.api.ConnectionParams;
import com.spotify.android.appremote.api.Connector;
import com.spotify.android.appremote.api.SpotifyAppRemote;

import com.spotify.protocol.client.Subscription;
import com.spotify.protocol.types.PlayerState;
import com.spotify.protocol.types.Track;
import com.sun.org.apache.xalan.internal.xsltc.compiler.Constants.REDIRECT_URI



class MainActivity : AppCompatActivity() {
    private val CLIENT_ID = "0f3440d322e34e36970d561e9d1b8ffd"
    private val REDIRECT_URI = "http://com.example.beetst/callback"
    private var mSpotifyAppRemote: SpotifyAppRemote? = null

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onStart()
    {
        super.onStart();
        // We will start writing our code here.
        val connectionParams = ConnectionParams.Builder(CLIENT_ID)
            .setRedirectUri(REDIRECT_URI)
            .showAuthView(true)
            .build()
        SpotifyAppRemote.connect(this, connectionParams,
            object : Connector.ConnectionListener {

                override fun onConnected(spotifyAppRemote: SpotifyAppRemote) {
                    mSpotifyAppRemote = spotifyAppRemote
                    Log.d("MainActivity", "Connected! Yay!")

                    // Now you can start interacting with App Remote
                    connected(mSpotifyAppRemote.getPlayerApi().play("spotify:playlist:37i9dQZF1DX2sUQwD7tbmL");)
                }

                override fun onFailure(throwable: Throwable) {
                    Log.e("MainActivity", throwable.message, throwable)

                    // Something went wrong when attempting to connect! Handle errors here
                }
            })
    }

    private void connected()
    {
        // Then we will write some more code here.
    }

    @Override
    protected void onStop()
    {
        super.onStop();
        // Aaand we will finish off here.
    }
}